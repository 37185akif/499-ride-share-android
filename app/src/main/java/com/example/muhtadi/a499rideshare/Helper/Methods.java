package com.example.muhtadi.a499rideshare.Helper;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.Toast;

import com.example.muhtadi.a499rideshare.R;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.mukesh.countrypicker.Country;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Methods {
    public static final long HOUR = 3600 * 1000;
    public void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public  boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public String getCountryCode(Context context) {
        String dialCode;
        try {
            Country country = Country.getCountryFromSIM(context);
            dialCode = country.getDialCode();
        } catch (Exception e) {
            dialCode = "+880";
        }

        return dialCode;
    }

    public void finishAllActivity(Activity context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            context.finishAffinity();
        } else {
            ActivityCompat.finishAffinity(context);
        }
    }

    public void cancelProgressDialog(ProgressDialog pd) {
        if (pd != null && pd.isShowing()) {
            pd.dismiss();
        }
    }

    public AlertDialog globalAlert(Context context, String text) {
        //Toast.makeText(context,text,Toast.LENGTH_LONG).show();
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.setTitle("RIDE SHARE");
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setMessage(text);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();

        return alertDialog;
    }



    public ProgressDialog showProgressDialog(final Activity context) {

        ProgressDialog pd = new ProgressDialog(context);

        pd.show();

        return pd;
    }


    public ProgressDialog showFindProgressDialog(final Activity context,String txt) {

        ProgressDialog pd = new ProgressDialog(context,txt);

        pd.show();

        return pd;
    }


    public CharSequence getRelativeTime(String date) {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Timestamp(System.currentTimeMillis()));
        CharSequence relativeTime = DateUtils.getRelativeTimeSpanString(
                new Methods().returnInMillisWithDate(date), new Methods().returnInMillis(timeStamp), 0);
        return relativeTime;
    }


    public long returnInMillis(String time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long millis = date.getTime();

        return millis;
    }


    public long returnInMillisWithDate(String time) {
        Log.d("time::::", time);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = format.parse(time);
            long millis = date.getTime() + 0 * HOUR;
            return millis;
        } catch (ParseException e) {
            Log.d("date::::", e.getMessage());
            return 0L;
        }
    }

    public boolean isPhoneNumberValid(String phoneNumber, String countryCode) {
        //NOTE: This should probably be a member variable.
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

        try {
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, countryCode);
            return phoneUtil.isValidNumber(numberProto);
        } catch (NumberParseException e) {

        }

        return false;
    }

}
