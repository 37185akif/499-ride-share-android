package com.example.muhtadi.a499rideshare.Adapter;

public class Constants {
    public static String MAP_INTENT_START_POINT = "intent_stps";
    public static String MAP_INTENT_USER = "intent_us";
    public static String SEARCH_RESULT_INTENT = "sch_rsp";
}
