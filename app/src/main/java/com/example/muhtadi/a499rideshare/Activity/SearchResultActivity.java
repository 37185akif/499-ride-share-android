package com.example.muhtadi.a499rideshare.Activity;

import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.muhtadi.a499rideshare.Adapter.RidePartnersAdapter;
import com.example.muhtadi.a499rideshare.Helper.Methods;
import com.example.muhtadi.a499rideshare.Model.SearchRideResponse;
import com.example.muhtadi.a499rideshare.Model.User;
import com.example.muhtadi.a499rideshare.Model.UserRide;
import com.example.muhtadi.a499rideshare.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.muhtadi.a499rideshare.Adapter.Constants.MAP_INTENT_USER;
import static com.example.muhtadi.a499rideshare.Adapter.Constants.SEARCH_RESULT_INTENT;

public class SearchResultActivity extends AppCompatActivity {
    @BindView(R.id.txtPickUp)
    TextView txtPickUp;

    @BindView(R.id.txtDestination)
    TextView txtDestination;

    @BindView(R.id.txtDistance)
    TextView txtDistance;

    @BindView(R.id.txtRideCode)
    TextView txtRideCode;

    @BindView(R.id.txtRideFare)
    TextView txtRideFare;


    @BindView(R.id.txtUserCount)
    TextView txtUserCount;

    @BindView(R.id.txtTime)
    TextView txtTime;


    @BindView(R.id.ride_partner_recyclerView)
    RecyclerView ride_partner_recyclerView;


    SearchRideResponse searchRideResponse;
    Gson gS = new Gson();
    public static final int MULTIPLE_PERMISSIONS = 10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Search Result");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        searchRideResponse = gS.fromJson(getIntent().getStringExtra(SEARCH_RESULT_INTENT), SearchRideResponse.class);

        txtPickUp.setText("Pick Point: " + searchRideResponse.getOwn().getRequest().getStart_point().getPlace_name());
        txtDestination.setText("Destination: " + searchRideResponse.getOwn().getRequest().getDestination());
        txtDistance.setText("Distance: " + searchRideResponse.getOwn().getRequest().getDistance() + " KM");
        txtRideCode.setText("Ride Code: " + searchRideResponse.getOwn().getShare_code());
        txtRideFare.setText("Fare: " + searchRideResponse.getOwn().getRide_fare() + " BDT");
        txtUserCount.setText(searchRideResponse.getOwn().getRequest().getUser_count() + " People");
        txtTime.setText(new Methods().getRelativeTime(searchRideResponse.getOwn().getPosted_on()));
        showRidePartners();
    }

    public void showRidePartners() {
        // custom dialog
        List<UserRide> rideList = searchRideResponse.getOther_user();
        RidePartnersAdapter mAdapter = new RidePartnersAdapter(rideList,SearchResultActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchResultActivity.this);
        ride_partner_recyclerView.setLayoutManager(mLayoutManager);
        ride_partner_recyclerView.setItemAnimator(new DefaultItemAnimator());
        ride_partner_recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permissions granted.
                    new Methods().showToast(this, getResources().getString(R.string.permission_success));
                } else {
                    // no permissions granted.
                    new Methods().showToast(this, getResources().getString(R.string.permission_error));
                }
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
