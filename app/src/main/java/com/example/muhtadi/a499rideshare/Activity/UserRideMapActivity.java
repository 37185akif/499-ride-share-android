package com.example.muhtadi.a499rideshare.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.muhtadi.a499rideshare.Helper.DirectionsJSONParser;
import com.example.muhtadi.a499rideshare.Helper.Methods;
import com.example.muhtadi.a499rideshare.Helper.Preference;
import com.example.muhtadi.a499rideshare.Helper.ProgressDialog;
import com.example.muhtadi.a499rideshare.Model.SearchRideResponse;
import com.example.muhtadi.a499rideshare.Model.StartingPoints;
import com.example.muhtadi.a499rideshare.Model.User;
import com.example.muhtadi.a499rideshare.Model.UserRequest;
import com.example.muhtadi.a499rideshare.R;
import com.example.muhtadi.a499rideshare.REST.ApiClient;
import com.example.muhtadi.a499rideshare.REST.ApiInterface;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.muhtadi.a499rideshare.Adapter.Constants.MAP_INTENT_START_POINT;
import static com.example.muhtadi.a499rideshare.Adapter.Constants.MAP_INTENT_USER;
import static com.example.muhtadi.a499rideshare.Adapter.Constants.SEARCH_RESULT_INTENT;

public class UserRideMapActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, PlaceSelectionListener {

    private GoogleMap mMap;
    Gson gS = new Gson();
    StartingPoints startingPoint;
    User user;
    GoogleApiClient mGoogleApiClient;
    List<LatLng> latLngList = new ArrayList<>();
    ///Polyline
    Polyline roadpolyline;
    public static String MY_API_KEY = "AIzaSyBYJXzIEZkuyI7XZXLGbmtJVnxYCQAQF6k";
    ProgressDialog pd;
    @BindView(R.id.btnFind)
    Button btnFind;
    String selected_place = "";
    Preference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_ride_map);
        ButterKnife.bind(this);
        preference = new Preference(this);
        startingPoint = gS.fromJson(getIntent().getStringExtra(MAP_INTENT_START_POINT), StartingPoints.class);
        user = gS.fromJson(getIntent().getStringExtra(MAP_INTENT_USER), User.class);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        btnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (latLngList.size() < 2) {
                    new Methods().globalAlert(UserRideMapActivity.this, "You have not choose your destination yet.");
                } else {
                    showSeatCounterDialog(selected_place);
                }
            }
        });
        if (preference.isSearchingRide()) {
            rideExistDialog("There is a pending request. You want to continue with that request or cancel ?");
        }
    }


    protected synchronized void buildGoogleApiClient() {
        //Toast.makeText(this, "buildGoogleApiClient", Toast.LENGTH_SHORT).show();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    public void locationSearch() {
        //AutoComplete search Filter setup
        Log.d("TAG:::", "in location search");
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                .setCountry("BD")
                .build();
        //initialization placeAutocomplete fragment widget
        final PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_fragment);
        autocompleteFragment.setHint("Enter Your Destination Here");
        autocompleteFragment.setFilter(typeFilter);
        autocompleteFragment.setMenuVisibility(true);
        autocompleteFragment.setBoundsBias(null);
        autocompleteFragment.setOnPlaceSelectedListener(this);
        autocompleteFragment.getView().findViewById(R.id.place_autocomplete_clear_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //example : way to access view from PlaceAutoCompleteFragment
                //((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setText("");
                autocompleteFragment.setText("");
                view.setVisibility(View.GONE);

            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng start_map_position = new LatLng(startingPoint.getLatitude(), startingPoint.getLongitude());
        mMap.addMarker(new MarkerOptions().position(start_map_position).title(user.getName() + " at " + startingPoint.getPlace_name()));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(start_map_position, 18));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        //mMap.animateCamera(CameraUpdateFactory.zoomTo(20));

        mMap.setBuildingsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        locationSearch();
        latLngList.add(0, start_map_position);
    }

    @Override
    protected void onStart() {
        buildGoogleApiClient();
        super.onStart();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onPlaceSelected(Place place) {
        LatLng destination = place.getLatLng();
        LatLng start_map_position = new LatLng(startingPoint.getLatitude(), startingPoint.getLongitude());
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(start_map_position).title(user.getName() + " at " + startingPoint.getPlace_name()));
        mMap.addMarker(new MarkerOptions().position(destination).title("Destination " + place.getName()));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(destination, 12));
        latLngList.add(1, destination);
        if (roadpolyline != null) {
            roadpolyline.remove();
        }
        drawDirectionPolyLine(latLngList.get(0), latLngList.get(1));
        selected_place = place.getName().toString();

    }


    // Draw Poly line Over road
    public void drawDirectionPolyLine(LatLng origin, LatLng destination) {

        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(origin, destination);

        DownloadTask downloadTask = new DownloadTask();

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);

    }


    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = downloadUrl(url[0]);
                Log.d("TAG:::", data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();


            parserTask.execute(result);

        }
    }


    // A class to parse the Google Places in JSON format
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        String distance = "";
        String duration = "";

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("TAG:::", e.getMessage());
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";


            if (result.size() < 1) {
//                Toast.makeText(getBaseContext(), "Too Short Distance ", Toast.LENGTH_SHORT).show();
                return;
            }
            Log.d("TAG:::", result.toString());

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {
                        // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;

                    } else if (j == 1) {

                        // Get duration from the list
                        duration = (String) point.get("duration");
                        Log.d("TAG:::", duration);
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.parseColor("#e64702"));

            }

            //set Distance and Duration in textview
            //raidDetails.setText("   Distance :  " + distance + " \n   Duration : " + duration);

            // Drawing polyline in the Google Map for the i-th route
            roadpolyline = mMap.addPolyline(lineOptions);
            Log.d("TAG:::", "add poly");
        }
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + MY_API_KEY;
        Log.d("TAG:::", "req url");

        return url;
    }


    //A method to download json data from url
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {

            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
            Log.d("TAG:::", "down complete");
        }
        return data;
    }


    @Override
    public void onError(Status status) {

    }


    public void showSeatCounterDialog(String place_name) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.user_count_layout_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edtSendingValue = dialogView.findViewById(R.id.edtCount);


        dialogBuilder.setTitle("Ride Share");
        dialogBuilder.setIcon(R.mipmap.ic_launcher);
//        dialogBuilder.setMessage("Please enter your " + type + " here which you used at the time of registering with Royalty Bangladesh.");
//        dialogBuilder.setMessage("Enter your "+type+" address and we'll send you a link to reset your username/password.");
        dialogBuilder.setMessage("How many seats you'll be needing while traveling to " + place_name + " ? You can request for maximum 3 seats.");
        //dialogBuilder.setMessage("Enter text below");
        dialogBuilder.setPositiveButton("Send Request", null);
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do nothing
            }
        });
        final AlertDialog b = dialogBuilder.create();
        b.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button btnPositive = b.getButton(AlertDialog.BUTTON_POSITIVE);
                btnPositive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String final_value = edtSendingValue.getText().toString();
                        if (final_value.equals("")) {
                            edtSendingValue.setError("Please enter how many seats do you need.");
                        } else if (Integer.parseInt(final_value) < 1 || Integer.parseInt(final_value) > 3) {
                            edtSendingValue.setError("You can request for minimum 1 seat and maximum 3 seats");
                        } else {
                            pd = new Methods().showProgressDialog(UserRideMapActivity.this);
                            createRideRequest(Integer.parseInt(final_value));
                            dialog.dismiss();
                        }

                    }
                });
            }
        });
        b.show();
    }


    public void createRideRequest(int user_count) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UserRequest> call = apiInterface.createRequest(user.getId(), startingPoint.getId(), selected_place,
                latLngList.get(1).longitude, latLngList.get(1).latitude, user_count);
        call.enqueue(new Callback<UserRequest>() {
            @Override
            public void onResponse(Call<UserRequest> call, final Response<UserRequest> response) {
                new Methods().cancelProgressDialog(pd);
                if (response.code() == 200) {
                    String data_response = gS.toJson(response.body());
                    preference.setRideSearching(true);
                    preference.setRideInfo(data_response);
                    pd = new Methods().showFindProgressDialog(UserRideMapActivity.this, "Searching your ride partner.");
                    Button btnCancel = pd.findViewById(R.id.btnCancel);
                    btnCancel.setVisibility(View.VISIBLE);
                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new Methods().cancelProgressDialog(pd);
                            pd = new Methods().showProgressDialog(UserRideMapActivity.this);
                            deleteRideRequest(response.body().getId());
                        }
                    });
                    searchRide(response.body().getId());
                } else if (response.code() == 201) {
                    rideExistDialog(response.body().getResult());
                } else {
                    new Methods().showToast(UserRideMapActivity.this, getResources().getString(R.string.error));
                }
            }

            @Override
            public void onFailure(Call<UserRequest> call, Throwable t) {
                new Methods().cancelProgressDialog(pd);
                new Methods().showToast(UserRideMapActivity.this, getResources().getString(R.string.error));
            }

        });
    }


    public void deleteRideRequest(int request_id) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UserRequest> call = apiInterface.deleteRequest(request_id);
        call.enqueue(new Callback<UserRequest>() {
            @Override
            public void onResponse(Call<UserRequest> call, Response<UserRequest> response) {
                new Methods().cancelProgressDialog(pd);
                if (response.code() == 200) {
                    new Methods().globalAlert(UserRideMapActivity.this, response.body().getResult());
                    preference.setRideSearching(false);
                } else {
                    new Methods().showToast(UserRideMapActivity.this, getResources().getString(R.string.error));
                }
            }

            @Override
            public void onFailure(Call<UserRequest> call, Throwable t) {
                Log.d("TAG:::", t.getMessage());
                new Methods().cancelProgressDialog(pd);
                new Methods().showToast(UserRideMapActivity.this, getResources().getString(R.string.error));
            }

        });
    }

    public void searchRide(final int request_id) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SearchRideResponse> call = apiInterface.searchRide(request_id);
        call.enqueue(new Callback<SearchRideResponse>() {
            @Override
            public void onResponse(Call<SearchRideResponse> call, Response<SearchRideResponse> response) {
                if (response.code() == 200) {
                    new Methods().cancelProgressDialog(pd);
                    preference.setRideSearching(false);
                    String data = gS.toJson(response.body());
                    Intent i = new Intent(UserRideMapActivity.this, SearchResultActivity.class);
                    i.putExtra(SEARCH_RESULT_INTENT, data);
                    startActivity(i);
                    finish();
                } else if (response.code() == 201) {
                    Handler handler = new Handler();
                    Runnable r = new Runnable() {
                        public void run() {
                            searchRide(request_id);
                        }
                    };
                    handler.postDelayed(r, 10000);
                } else {
                    new Methods().cancelProgressDialog(pd);
                    new Methods().showToast(UserRideMapActivity.this, getResources().getString(R.string.error));
                }
            }

            @Override
            public void onFailure(Call<SearchRideResponse> call, Throwable t) {
                Log.d("TAG:::", t.getMessage());
                new Methods().cancelProgressDialog(pd);
                new Methods().showToast(UserRideMapActivity.this, getResources().getString(R.string.error));
            }

        });
    }


    public void rideExistDialog(String text) {
        AlertDialog alertDialog = new AlertDialog.Builder(UserRideMapActivity.this).create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.setTitle("Ride Share");
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setMessage(text);
        final UserRequest userRequest = gS.fromJson(preference.getRideInfo(), UserRequest.class);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel Request",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        pd = new Methods().showProgressDialog(UserRideMapActivity.this);
                        deleteRideRequest(userRequest.getId());
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Continue",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pd = new Methods().showFindProgressDialog(UserRideMapActivity.this, "Searching your ride partner.");
                        Button btnCancel = pd.findViewById(R.id.btnCancel);
                        btnCancel.setVisibility(View.VISIBLE);
                        btnCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                new Methods().cancelProgressDialog(pd);
                                pd = new Methods().showProgressDialog(UserRideMapActivity.this);
                                deleteRideRequest(userRequest.getId());
                            }
                        });
                        searchRide(userRequest.getId());
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

}
