package com.example.muhtadi.a499rideshare.Activity;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.example.muhtadi.a499rideshare.Helper.Methods;
import com.example.muhtadi.a499rideshare.Helper.Preference;
import com.example.muhtadi.a499rideshare.Helper.ProgressDialog;
import com.example.muhtadi.a499rideshare.Model.User;
import com.example.muhtadi.a499rideshare.R;
import com.example.muhtadi.a499rideshare.REST.ApiClient;
import com.example.muhtadi.a499rideshare.REST.ApiInterface;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.edtFirstName)
    TextInputLayout edtFirstName;

    @BindView(R.id.edtEmail)
    TextInputLayout edtEmail;

    @BindView(R.id.edtPhone)
    TextInputLayout edtPhone;

    @BindView(R.id.edtAddress)
    TextInputLayout edtAddress;

    @BindView(R.id.edtStudentID)
    TextInputLayout edtStudentID;

    @BindView(R.id.edtPassword)
    TextInputLayout edtPassword;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    ProgressDialog pd;
    Preference preference;
    Gson gS = new Gson();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        preference = new Preference(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Registration");
        edtPhone.getEditText().append(new Methods().getCountryCode(this));
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                    pd = new Methods().showProgressDialog(RegisterActivity.this);
                    register();
                }
            }
        });
    }



    public void register() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<User> call = apiInterface.createUser(edtFirstName.getEditText().getText().toString(),
                edtEmail.getEditText().getText().toString(),
                edtPhone.getEditText().getText().toString(),
                edtPassword.getEditText().getText().toString(),
                edtAddress.getEditText().getText().toString(),
                edtStudentID.getEditText().getText().toString());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                new Methods().cancelProgressDialog(pd);
                if (response.code() == 200) {
                    String data_response = gS.toJson(response.body());
                    preference.setLoggedIn(true);
                    preference.setUserData(data_response);
                    startActivity(new Intent(RegisterActivity.this,HomeActivity.class));
                    new Methods().finishAllActivity(RegisterActivity.this);
                } else if (response.code() == 201) {
                    new Methods().globalAlert(RegisterActivity.this, response.body().getResult());
                } else {
                    new Methods().showToast(RegisterActivity.this, getResources().getString(R.string.error));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                new Methods().cancelProgressDialog(pd);
                new Methods().showToast(RegisterActivity.this, getResources().getString(R.string.error));
            }

        });
    }


    public boolean validate(){
        if(edtFirstName.getEditText().getText().length()<1){
            edtFirstName.getEditText().setError("Please enter your name correctly");
            return false;
        }else{
            edtFirstName.getEditText().setError(null);
        }

        if(!new Methods().isValidEmail(edtEmail.getEditText().getText())){
            edtEmail.getEditText().setError("Please enter your email correctly");
            return false;
        }else{
            edtEmail.getEditText().setError(null);
        }

        if (!new Methods().isPhoneNumberValid(edtPhone.getEditText().getText().toString(),
                new Methods().getCountryCode(RegisterActivity.this))) {
            edtPhone.setError("Please enter your phone number correctly.");
            return false;
        }else{
            edtPhone.getEditText().setError(null);
        }

        if(edtAddress.getEditText().getText().length()<1){
            edtAddress.getEditText().setError("Please enter your address correctly");
            return false;
        }else{
            edtAddress.getEditText().setError(null);
        }

        if(edtStudentID.getEditText().getText().length()<10){
            edtStudentID.getEditText().setError("Please enter your ID correctly");
            return false;
        }else{
            edtStudentID.getEditText().setError(null);
        }
        if(edtPassword.getEditText().getText().length()<6){
            edtPassword.getEditText().setError("Your password has to be minimum 6 characters");
            return false;
        }else{
            edtPassword.getEditText().setError(null);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
