package com.example.muhtadi.a499rideshare.Model;

import com.google.gson.annotations.SerializedName;

public class StartingPoints {

    @SerializedName("id")
    private int id;

    @SerializedName("place_name")
    private String place_name;

    @SerializedName("longitude")
    private double longitude;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlace_name() {
        return place_name;
    }

    public void setPlace_name(String place_name) {
        this.place_name = place_name;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @SerializedName("latitude")
    private double latitude;
}
