package com.example.muhtadi.a499rideshare.Adapter;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.muhtadi.a499rideshare.Model.UserRide;
import com.example.muhtadi.a499rideshare.R;

import java.util.List;

public class RidePartnersAdapter extends RecyclerView.Adapter<RidePartnersAdapter.MyViewHolder> {

    private List<UserRide> rideList;
    Activity mActivity;
    public static final int MULTIPLE_PERMISSIONS = 10;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;
        TextView txtPhone;
        TextView txtDestination;
        TextView txtDistance;
        TextView txtUserCount;
        TextView txtRideFare;
        Button btnCall;

        public MyViewHolder(View view) {
            super(view);
            txtName = view.findViewById(R.id.txtName);
            txtPhone = view.findViewById(R.id.txtPhone);
            txtDestination = view.findViewById(R.id.txtDestination);
            txtDistance = view.findViewById(R.id.txtDistance);
            txtRideFare = view.findViewById(R.id.txtRideFare);
            txtUserCount = view.findViewById(R.id.txtUserCount);
            btnCall = view.findViewById(R.id.btnCall);
        }
    }


    public RidePartnersAdapter(List<UserRide> rideList,Activity mActivity) {
        this.rideList = rideList;
        this.mActivity = mActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rider_partners_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final UserRide userRide = rideList.get(position);
        holder.txtName.setText("Name: " + userRide.getRequest().getUser().getName());
        holder.txtPhone.setText("Phone: " + userRide.getRequest().getUser().getPhone());
        holder.txtDestination.setText("Destination: " + userRide.getRequest().getDestination());
        holder.txtDistance.setText("Distance: " + userRide.getRequest().getDistance() + " KM");
        holder.txtRideFare.setText("Fare: " + userRide.getRide_fare() + " BDT");
        holder.txtUserCount.setText(userRide.getRequest().getUser_count() + " People");
        holder.btnCall.setText("call " + userRide.getRequest().getUser().getName());
        holder.btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callPartner(userRide.getRequest().getUser().getPhone());
            }
        });
    }

    public void callPartner(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel: " + phoneNumber));
        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            checkPermission();
            return;
        } else {
            mActivity.startActivity(intent);
        }
    }

    public boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(mActivity,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity,
                    Manifest.permission.CALL_PHONE)) {


                ActivityCompat.requestPermissions(mActivity,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MULTIPLE_PERMISSIONS);


            } else {
                ActivityCompat.requestPermissions(mActivity,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MULTIPLE_PERMISSIONS);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public int getItemCount() {
        return rideList.size();
    }
}