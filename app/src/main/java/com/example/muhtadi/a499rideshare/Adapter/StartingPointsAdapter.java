package com.example.muhtadi.a499rideshare.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.muhtadi.a499rideshare.Activity.UserRideMapActivity;
import com.example.muhtadi.a499rideshare.Helper.Preference;
import com.example.muhtadi.a499rideshare.Helper.ProgressDialog;
import com.example.muhtadi.a499rideshare.Model.StartingPoints;
import com.example.muhtadi.a499rideshare.R;
import com.google.gson.Gson;


import java.util.List;

import static com.example.muhtadi.a499rideshare.Adapter.Constants.MAP_INTENT_START_POINT;
import static com.example.muhtadi.a499rideshare.Adapter.Constants.MAP_INTENT_USER;

public class StartingPointsAdapter extends RecyclerView.Adapter<StartingPointsAdapter.MyViewHolder> {

    private List<StartingPoints> startingPoints;
    private Activity mContext;
    Dialog dialog;
    Gson gS = new Gson();
    Preference preference;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtBranch;
        LinearLayout info_holder;
        View divider;

        public MyViewHolder(View view) {
            super(view);
            txtBranch = view.findViewById(R.id.txtBranch);
            info_holder = view.findViewById(R.id.holder);
            divider = view.findViewById(R.id.divider);
        }
    }


    public StartingPointsAdapter(List<StartingPoints> startingPoints, Activity mContext, Dialog dialog) {
        this.startingPoints = startingPoints;
        this.mContext = mContext;
        this.dialog = dialog;
        preference = new Preference(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.starting_point_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final StartingPoints startingPoint = startingPoints.get(position);
        if (position == getItemCount() - 1) {
            holder.divider.setVisibility(View.GONE);
        }
        holder.txtBranch.setText(startingPoint.getPlace_name());
        holder.info_holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String starting_point_data = gS.toJson(startingPoint);
                Intent i = new Intent(mContext, UserRideMapActivity.class);
                i.putExtra(MAP_INTENT_START_POINT, starting_point_data);
                i.putExtra(MAP_INTENT_USER, preference.getUserPref());
                mContext.startActivity(i);
                dialog.dismiss();
            }
        });

    }

    @Override
    public int getItemCount() {
        return startingPoints.size();
    }
}