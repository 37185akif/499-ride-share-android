package com.example.muhtadi.a499rideshare.Model;

import com.google.gson.annotations.SerializedName;

public class UserRequest {

    @SerializedName("id")
    private int id;

    @SerializedName("ride_status")
    private int ride_status;

    @SerializedName("distance")
    private double distance;

    @SerializedName("user_count")
    private int user_count;

    @SerializedName("latitude")
    private double latitude;

    @SerializedName("longitude")
    private double longitude;

    @SerializedName("destination")
    private String destination;

    @SerializedName("posted_on")
    private String posted_on;

    public StartingPoints getStart_point() {
        return start_point;
    }

    public void setStart_point(StartingPoints start_point) {
        this.start_point = start_point;
    }

    @SerializedName("start_point")
    private StartingPoints start_point;

    public String getPosted_on() {
        return posted_on;
    }

    public void setPosted_on(String posted_on) {
        this.posted_on = posted_on;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @SerializedName("user")
    private User user;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @SerializedName("result")
    private String result;

    @SerializedName("start_point_id")
    private int start_point_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRide_status() {
        return ride_status;
    }

    public void setRide_status(int ride_status) {
        this.ride_status = ride_status;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getUser_count() {
        return user_count;
    }

    public void setUser_count(int user_count) {
        this.user_count = user_count;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getStart_point_id() {
        return start_point_id;
    }

    public void setStart_point_id(int start_point_id) {
        this.start_point_id = start_point_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    @SerializedName("user_id")
    private int user_id;
}
