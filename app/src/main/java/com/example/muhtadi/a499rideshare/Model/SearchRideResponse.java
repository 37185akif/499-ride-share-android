package com.example.muhtadi.a499rideshare.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchRideResponse {
    @SerializedName("own")
    private UserRide own;

    public UserRide getOwn() {
        return own;
    }

    public void setOwn(UserRide own) {
        this.own = own;
    }

    public List<UserRide> getOther_user() {
        return other_user;
    }

    public void setOther_user(List<UserRide> other_user) {
        this.other_user = other_user;
    }

    @SerializedName("other_user")
    private List<UserRide> other_user;
}
