package com.example.muhtadi.a499rideshare.Activity;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.muhtadi.a499rideshare.Helper.Methods;
import com.example.muhtadi.a499rideshare.Helper.Preference;
import com.example.muhtadi.a499rideshare.Helper.ProgressDialog;
import com.example.muhtadi.a499rideshare.Model.User;
import com.example.muhtadi.a499rideshare.R;
import com.example.muhtadi.a499rideshare.REST.ApiClient;
import com.example.muhtadi.a499rideshare.REST.ApiInterface;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.edtPhone)
    TextInputLayout edtPhone;

    @BindView(R.id.edtPassword)
    TextInputLayout edtPassword;

    @BindView(R.id.btnLogin)
    Button btnLogin;

    ProgressDialog pd;
    Gson gS = new Gson();
    Preference preference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Login");
        preference = new Preference(this);
        edtPhone.getEditText().append("+880");
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd = new Methods().showProgressDialog(LoginActivity.this);
                login();
            }
        });
    }

    public void login() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<User> call = apiInterface.login(edtPhone.getEditText().getText().toString(),
                edtPassword.getEditText().getText().toString());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                new Methods().cancelProgressDialog(pd);
                if (response.code() == 200) {
                    String data_response = gS.toJson(response.body());
                    preference.setLoggedIn(true);
                    preference.setUserData(data_response);
                    startActivity(new Intent(LoginActivity.this,HomeActivity.class));
                    new Methods().finishAllActivity(LoginActivity.this);
                } else if (response.code() == 201) {
                    new Methods().globalAlert(LoginActivity.this, response.body().getResult());
                } else {
                    new Methods().showToast(LoginActivity.this, getResources().getString(R.string.error));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                new Methods().cancelProgressDialog(pd);
                new Methods().showToast(LoginActivity.this, getResources().getString(R.string.error));
            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
