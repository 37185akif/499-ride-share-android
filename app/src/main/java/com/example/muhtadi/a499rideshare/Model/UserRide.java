package com.example.muhtadi.a499rideshare.Model;

import com.google.gson.annotations.SerializedName;

public class UserRide {
    @SerializedName("id")
    private int id;

    @SerializedName("request_id")
    private int request_id;

    @SerializedName("ride_fare")
    private int ride_fare;

    @SerializedName("share_code")
    private String share_code;

    @SerializedName("posted_on")
    private String posted_on;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRequest_id() {
        return request_id;
    }

    public void setRequest_id(int request_id) {
        this.request_id = request_id;
    }

    public int getRide_fare() {
        return ride_fare;
    }

    public void setRide_fare(int ride_fare) {
        this.ride_fare = ride_fare;
    }

    public String getShare_code() {
        return share_code;
    }

    public void setShare_code(String share_code) {
        this.share_code = share_code;
    }

    public String getPosted_on() {
        return posted_on;
    }

    public void setPosted_on(String posted_on) {
        this.posted_on = posted_on;
    }

    public UserRequest getRequest() {
        return request;
    }

    public void setRequest(UserRequest request) {
        this.request = request;
    }

    @SerializedName("request")
    private UserRequest request;
}
