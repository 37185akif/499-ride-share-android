package com.example.muhtadi.a499rideshare.Helper;

import android.content.Context;
import android.content.SharedPreferences;

public class Preference {
    public static final String USER_PREF = "user_data";
    public static final String LOGGED_IN = "lg";
    public static final String RIDE_SEARCHING = "rd_rs";
    public static final String RIDE_INFO = "rd_inf";


    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    public Preference(Context context) {
        sharedPreferences = context.getSharedPreferences("royalty_preference", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }


    public void setRideInfo(String data) {
        editor.putString(RIDE_INFO, data);
        editor.apply();
        editor.commit();
    }

    public void setUserData(String data) {
        editor.putString(USER_PREF, data);
        editor.apply();
        editor.commit();
    }


    public String getUserPref() {
        return sharedPreferences.getString(USER_PREF, "");
    }

    public String getRideInfo() {
        return sharedPreferences.getString(RIDE_INFO, "");
    }

    public boolean loggedIn() {
        return sharedPreferences.getBoolean(LOGGED_IN, false);
    }

    public boolean isSearchingRide() {
        return sharedPreferences.getBoolean(RIDE_SEARCHING, false);
    }

    public void setLoggedIn(boolean status) {
        editor.putBoolean(LOGGED_IN, status);
        editor.apply();
        editor.commit();
    }

    public void setRideSearching(boolean status) {
        editor.putBoolean(RIDE_SEARCHING, status);
        editor.apply();
        editor.commit();
    }



}
