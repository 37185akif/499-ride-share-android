package com.example.muhtadi.a499rideshare.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.muhtadi.a499rideshare.Adapter.StartingPointsAdapter;
import com.example.muhtadi.a499rideshare.Helper.Methods;
import com.example.muhtadi.a499rideshare.Helper.Preference;
import com.example.muhtadi.a499rideshare.Helper.ProgressDialog;
import com.example.muhtadi.a499rideshare.Model.SearchRideResponse;
import com.example.muhtadi.a499rideshare.Model.StartingPoints;
import com.example.muhtadi.a499rideshare.Model.User;
import com.example.muhtadi.a499rideshare.Model.UserRequest;
import com.example.muhtadi.a499rideshare.R;
import com.example.muhtadi.a499rideshare.REST.ApiClient;
import com.example.muhtadi.a499rideshare.REST.ApiInterface;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.muhtadi.a499rideshare.Adapter.Constants.SEARCH_RESULT_INTENT;

public class HomeActivity extends AppCompatActivity {
    Preference preference;

    @BindView(R.id.txtName)
    TextView txtName;

    @BindView(R.id.txtEmail)
    TextView txtEmail;

    @BindView(R.id.txtPhone)
    TextView txtPhone;

    @BindView(R.id.txtID)
    TextView txtID;

    @BindView(R.id.txtAddress)
    TextView txtAddress;

    @BindView(R.id.btnLogout)
    Button btnLogout;

    @BindView(R.id.btnFind)
    Button btnFind;

    @BindView(R.id.btnLastRide)
    Button btnLastRide;

    User user;
    Gson gS = new Gson();
    ProgressDialog pd;
    int TYPE_LAST_RIDE = 332;
    int NORMAL_RIDE = 323;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        preference = new Preference(this);
        getSupportActionBar().setTitle("Home");

        user = gS.fromJson(preference.getUserPref(), User.class);
        txtName.setText(user.getName());
        txtEmail.setText(user.getEmail());
        txtPhone.setText(user.getPhone());
        txtID.setText("NSU-ID: " + user.getStudent_id());
        txtAddress.setText("Address: " + user.getAddress());
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                accountLogOut(HomeActivity.this, "Are you sure you want to log out ?");
            }
        });

        btnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd = new Methods().showProgressDialog(HomeActivity.this);
                showStartingPoints();
            }
        });

        btnLastRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final UserRequest userRequest = gS.fromJson(preference.getRideInfo(), UserRequest.class);
                if (userRequest == null) {
                    new Methods().globalAlert(HomeActivity.this, "Sorry there is no last ride result to show");
                } else {
                    pd = new Methods().showProgressDialog(HomeActivity.this);
                    searchRide(userRequest.getId(),TYPE_LAST_RIDE);
                }
            }
        });

        if (preference.isSearchingRide()) {
            rideExistDialog("There is a pending request. You want to continue with that request or cancel ?");
        }
    }


    public void rideExistDialog(String text) {
        AlertDialog alertDialog = new AlertDialog.Builder(HomeActivity.this).create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.setTitle("Ride Share");
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setMessage(text);
        final UserRequest userRequest = gS.fromJson(preference.getRideInfo(), UserRequest.class);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel Request",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pd = new Methods().showProgressDialog(HomeActivity.this);
                        deleteRideRequest(userRequest.getId());
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Continue",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pd = new Methods().showFindProgressDialog(HomeActivity.this, "Searching your ride partner.");
                        Button btnCancel = pd.findViewById(R.id.btnCancel);
                        btnCancel.setVisibility(View.VISIBLE);
                        btnCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                new Methods().cancelProgressDialog(pd);
                                pd = new Methods().showProgressDialog(HomeActivity.this);
                                deleteRideRequest(userRequest.getId());
                            }
                        });
                        searchRide(userRequest.getId(),NORMAL_RIDE);
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    public void searchRide(final int request_id, final int type) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SearchRideResponse> call = apiInterface.searchRide(request_id);
        call.enqueue(new Callback<SearchRideResponse>() {
            @Override
            public void onResponse(Call<SearchRideResponse> call, Response<SearchRideResponse> response) {
                if (response.code() == 200) {
                    new Methods().cancelProgressDialog(pd);
                    preference.setRideSearching(false);
                    String data = gS.toJson(response.body());
                    Intent i = new Intent(HomeActivity.this, SearchResultActivity.class);
                    i.putExtra(SEARCH_RESULT_INTENT, data);
                    startActivity(i);
                    if(type!=TYPE_LAST_RIDE){
                        finish();
                    }
                } else if (response.code() == 201) {
                    Handler handler = new Handler();
                    Runnable r = new Runnable() {
                        public void run() {
                            searchRide(request_id,NORMAL_RIDE);
                        }
                    };
                    handler.postDelayed(r, 10000);
                } else {
                    new Methods().cancelProgressDialog(pd);
                    new Methods().showToast(HomeActivity.this, getResources().getString(R.string.error));
                }
            }

            @Override
            public void onFailure(Call<SearchRideResponse> call, Throwable t) {
                Log.d("TAG:::", t.getMessage());
                new Methods().cancelProgressDialog(pd);
                new Methods().showToast(HomeActivity.this, getResources().getString(R.string.error));
            }

        });
    }

    public void deleteRideRequest(int request_id) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UserRequest> call = apiInterface.deleteRequest(request_id);
        call.enqueue(new Callback<UserRequest>() {
            @Override
            public void onResponse(Call<UserRequest> call, Response<UserRequest> response) {
                new Methods().cancelProgressDialog(pd);
                if (response.code() == 200) {
                    new Methods().globalAlert(HomeActivity.this, response.body().getResult());
                    preference.setRideSearching(false);
                } else {
                    new Methods().showToast(HomeActivity.this, getResources().getString(R.string.error));
                }
            }

            @Override
            public void onFailure(Call<UserRequest> call, Throwable t) {
                Log.d("TAG:::", t.getMessage());
                new Methods().cancelProgressDialog(pd);
                new Methods().showToast(HomeActivity.this, getResources().getString(R.string.error));
            }

        });
    }


    public void showStartingPoints() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<List<StartingPoints>> call = apiInterface.getStartingPoints();
        call.enqueue(new Callback<List<StartingPoints>>() {
            @Override
            public void onResponse(Call<List<StartingPoints>> call, Response<List<StartingPoints>> response) {
                new Methods().cancelProgressDialog(pd);
                if (response.code() == 200) {
                    List<StartingPoints> startingPoints = response.body();
                    showStartingPointsDialog(startingPoints);
                } else {
                    new Methods().showToast(HomeActivity.this, getResources().getString(R.string.error));
                }
            }

            @Override
            public void onFailure(Call<List<StartingPoints>> call, Throwable t) {
                new Methods().cancelProgressDialog(pd);
                new Methods().showToast(HomeActivity.this, getResources().getString(R.string.error));
            }

        });
    }

    public void showStartingPointsDialog(List<StartingPoints> startingPoints) {
        // custom dialog
        Dialog dialog = new Dialog(HomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.starting_point_list_dialog_layout);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        StartingPointsAdapter mAdapter = new StartingPointsAdapter(startingPoints, HomeActivity.this, dialog);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(HomeActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        dialog.show();
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                new Methods().cancelProgressDialog(pd);
            }
        });
    }


    public void accountLogOut(final Activity context, String text) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Ride Share");
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setMessage(text);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        preference.setLoggedIn(false);
                        preference.setRideSearching(false);
                        startActivity(new Intent(HomeActivity.this, RegistrationActivity.class));
                        finish();
                    }
                });
        alertDialog.show();
    }

}
