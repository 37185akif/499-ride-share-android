package com.example.muhtadi.a499rideshare.REST;

import com.example.muhtadi.a499rideshare.Model.SearchRideResponse;
import com.example.muhtadi.a499rideshare.Model.StartingPoints;
import com.example.muhtadi.a499rideshare.Model.User;
import com.example.muhtadi.a499rideshare.Model.UserRequest;


import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {

    @FormUrlEncoded
    @POST("createUser.json")
    Call<User> createUser(@Field("name") String name,
                          @Field("email") String email,
                          @Field("phone") String phone,
                          @Field("password") String password,
                          @Field("address") String address,
                          @Field("student_id") String student_id);

    @FormUrlEncoded
    @POST("login.json")
    Call<User> login(@Field("phone") String phone,
                     @Field("password") String password);

    @GET("getStartingPoints.json")
    Call<List<StartingPoints>> getStartingPoints();

    @FormUrlEncoded
    @POST("createRequest.json")
    Call<UserRequest> createRequest(@Field("user_id") int user_id,
                                    @Field("start_point_id") int start_point_id,
                                    @Field("destination") String destination,
                                    @Field("longitude") double longitude,
                                    @Field("latitude") double latitude,
                                    @Field("user_count") int user_count);

    @FormUrlEncoded
    @POST("deleteRide.json")
    Call<UserRequest> deleteRequest(@Field("request_id") int request_id);

    @FormUrlEncoded
    @POST("searchRide.json")
    Call<SearchRideResponse> searchRide(@Field("request_id") int request_id);


}